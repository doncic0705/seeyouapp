import 'package:flutter/material.dart';
import 'package:see_you_app/components/common/component_text_btn.dart';
import 'package:see_you_app/config/config_color.dart';
import 'package:see_you_app/config/config_style.dart';

class PageAttendance extends StatefulWidget {
  const PageAttendance({super.key});

  @override
  State<PageAttendance> createState() => _PageAttendanceState();
}

class _PageAttendanceState extends State<PageAttendance> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          child: Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: colorLightGray),
              borderRadius: BorderRadius.circular(buttonRadius),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ComponentTextBtn("출근", () {}),
                    ComponentTextBtn("퇴근", () {}),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
